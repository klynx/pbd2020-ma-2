package si.uni_lj.fri.pbd.miniapp2;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;

public class AccelerationService extends Service implements SensorEventListener {
    private final static String TAG = AccelerationService.class.getSimpleName();
    IBinder accelerationBinder = (IBinder) new AccelerationBinder();

    public final static int MSG_UPDATE_TIME = 0;
    public final static String COMMAND_IDLE = "IDLE";
    public final static String COMMAND_HORIZONTAL = "HORIZONTAL";
    public final static String COMMAND_VERTICAL = "VERTICAL";

    private SensorManager accelerationManager;
    private Sensor accelerationSensor;
    private SensorForwardHandler sensorForwardHandler;
    private MediaPlayerService mediaPlayerService;

    private double prevX = 0.0;
    private double prevY = 0.0;
    private double prevZ = 0.0;
    private final double noise_threshold = 5.0;
    public String command = COMMAND_IDLE;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "returning binder");
        return accelerationBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind");
        return super.onUnbind(intent);
    }

    /**
     * gets MediaServicePlayer instance to which it sends playback commands based on gestures
     * @param mediaPlayerService instance that binds this service
     */
    public void registerClient(MediaPlayerService mediaPlayerService) {
        this.mediaPlayerService = mediaPlayerService;
    }

    class AccelerationBinder extends Binder {
        AccelerationService getService() {
            return AccelerationService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        accelerationManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        if (accelerationManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            accelerationSensor = accelerationManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            Log.d(TAG, "got acceleration sensor");
        }
        else
            Log.e(TAG, "no acceleration sensor!");

        accelerationManager.registerListener(this, accelerationSensor, SensorManager.SENSOR_DELAY_NORMAL);
        sensorForwardHandler = new SensorForwardHandler(this);
        sensorForwardHandler.sendEmptyMessage(MSG_UPDATE_TIME);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.values.length < 3) {
            Log.d(TAG, "not enough parameters");
            return;
        }

        double X = event.values[0];
        double Y = event.values[1];
        double Z = event.values[2];

        double dX = Math.abs(prevX - X);
        double dY = Math.abs(prevY - Y);
        double dZ = Math.abs(prevZ - Z);

        prevX = X;
        prevY = Y;
        prevZ = Z;

        if (dX <= noise_threshold && dY <= noise_threshold && dZ <= noise_threshold) {
            command = COMMAND_IDLE;
            return;
        }

        if (dX > dZ && (dX > noise_threshold || dZ > noise_threshold)) {
            command = COMMAND_HORIZONTAL;
            return;
        }

        if (dZ > dX && (dX > noise_threshold || dZ > noise_threshold)) {
            command = COMMAND_VERTICAL;
        }
    }

    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        accelerationManager.unregisterListener(this);
        sensorForwardHandler.removeMessages(MSG_UPDATE_TIME);
        super.onDestroy();
    }

    // needs to be here, because the interface requires it
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    /**
     * Handler that checks command every 500ms and sends playback information to mediaPlayService if necessary
     */
    private static class SensorForwardHandler extends Handler {
        WeakReference<AccelerationService> service;
        private int UPDATE_RATE_MS = 500;

        SensorForwardHandler(AccelerationService service) {
            this.service = new WeakReference<AccelerationService>(service);
        }

        @Override
        public void handleMessage(Message message) {
            if (message.what == MSG_UPDATE_TIME) {
                Log.d(TAG, service.get().command);

                if (service.get().command.equals(AccelerationService.COMMAND_HORIZONTAL)) {
                    service.get().mediaPlayerService.Pause();
                }

                if (service.get().command.equals(AccelerationService.COMMAND_VERTICAL)) {
                    service.get().mediaPlayerService.Play();
                }
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS);
            }
        }
    }
}
