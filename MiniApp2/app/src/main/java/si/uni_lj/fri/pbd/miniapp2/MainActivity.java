package si.uni_lj.fri.pbd.miniapp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();
    private MediaPlayerService mediaPlayerService;
    private boolean mediaPlayerBound = false;
    public TextView songInfo;
    public TextView songDuration;

    /**
     * ServiceConnection that binds MediaPlayerService
     */
    private ServiceConnection mediaPlayerServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected");
            try {
                MediaPlayerService.MediaPlayerBinder binder = (MediaPlayerService.MediaPlayerBinder) service;
                mediaPlayerService = binder.getService();
                Log.d(TAG, "Media player Service bound");
                mediaPlayerBound = true;

                // send this instance to service, so that it can update the UI
                registerSelf();
                Log.d(TAG, "registered activity in client");
                Log.d(TAG, "playing");
                playButtonOnClick(null);
            } catch (Exception e) {
                Log.e(TAG, "Error binding MediaPlayerService");
                Log.e(TAG, e.toString());
                mediaPlayerBound = false;
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "Media player Service unbound");
            mediaPlayerBound = false;
        }
    };

    /**
     * sends this instance to MediaPlayerService
     */
    private void registerSelf() {
        if (mediaPlayerBound)
            mediaPlayerService.registerClient(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            songInfo = (TextView) findViewById(R.id.songInfo);
            songDuration = (TextView) findViewById(R.id.songDuration);
        } catch (ClassCastException e ) {
            Log.e(TAG, "Error casting textViews");
            Log.e(TAG, e.toString());
        }
    }

    private void stopAndUnbindMediaService() {
        stopService(new Intent(this, MediaPlayerService.class));
        unbindService(mediaPlayerServiceConnection);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if (mediaPlayerBound)
            stopAndUnbindMediaService();
    }

    public void gesturesOn(View v) {
        if (mediaPlayerBound)
            mediaPlayerService.gesturesOn();
        else
            Toast.makeText(this, R.string.gestures_no_bound_service_toast, Toast.LENGTH_SHORT).show();
    }

    public void gesturesOff(View v) {
        if (mediaPlayerBound)
            mediaPlayerService.gesturesOff();
        else
            Toast.makeText(this, R.string.gestures_no_bound_service_toast, Toast.LENGTH_SHORT).show();
    }

    private boolean startAndBindMediaPlayerService() {
        try {
            Log.d(TAG,"Starting and binding service");
            Intent mediaPlayerIntent = new Intent(this, MediaPlayerService.class);
            mediaPlayerIntent.setAction(MediaPlayerService.ACTION_START);

            startService(mediaPlayerIntent);
            bindService(mediaPlayerIntent, mediaPlayerServiceConnection, 0);
            return true;
        } catch(Exception e) {
            Log.e(TAG, "Error starting and binding service");
            Log.e(TAG, e.toString());
            return false;
        }
    }

    public void playButtonOnClick(View v) {
        Log.d(TAG, "got play click");
        if (!mediaPlayerBound) {
            Log.d(TAG, "Service is not yet started and bound. Starting now!");
            if (!startAndBindMediaPlayerService()) {
                Toast.makeText(getApplication(), R.string.play_songs_failure, Toast.LENGTH_LONG).show();
            }
            Log.d(TAG, "Service not bound. Exiting");
            return;
        }
        mediaPlayerService.Play();
    }

    public void exitButtonOnClick(View v) {
        Log.d(TAG, "got exit click");
        if (mediaPlayerBound)
            mediaPlayerService.Exit();
        else
            finish();
    }

    public void pauseButtonOnClick(View v) {
        Log.d(TAG, "got pause click");
        if (mediaPlayerBound) {
            mediaPlayerService.Pause();
        }
    }

    public void stopButtonOnClick(View v) {
        Log.d(TAG, "got stop click");
        if (mediaPlayerBound) {
            mediaPlayerService.Stop();
        }
    }
}
