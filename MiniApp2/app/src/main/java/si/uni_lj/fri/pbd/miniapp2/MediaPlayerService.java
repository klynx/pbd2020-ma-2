package si.uni_lj.fri.pbd.miniapp2;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Pattern;

public class MediaPlayerService extends Service {
    // actions for starting from intent
    public static final String ACTION_START = "start_service";
    public static final String ACTION_STOP = "stop_song";
    public static final String ACTION_PLAY_PAUSE = "play_pause_song";
    public static final String ACTION_EXIT = "exit_service";
    private static final int NOTIFICATION_ID = 1337;
    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;

    private MainActivity mainActivity;
    private String songDuration;

    public static final String channelID = "song_playback_notification";

    public final static int MSG_UPDATE_TIME = 0;
    private SongUIHandler songUIHandler = new SongUIHandler(this);
    private AccelerationService accelerationService;
    private boolean serviceBound;

    String TAG = MediaPlayerService.class.getSimpleName();

    IBinder serviceBinder = (IBinder) new MediaPlayerBinder();
    private boolean isPaused = false;


    MediaPlayer audioPlayer;
    String[] songs;
    String currentSong = null;

    /**
     * ServiceConnection for binding AccelerationService
     */
    private ServiceConnection accelerationServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected");
            try {
                AccelerationService.AccelerationBinder binder = (AccelerationService.AccelerationBinder) service;
                accelerationService = binder.getService();

                // registers this Service to the AccelerationActivity so that it can receive movement information
                registerSelf();
                serviceBound = true;
                Log.d(TAG, "AccelerationService bound");
            } catch (Exception e) {
                Log.e(TAG, "Error binding AccelerationService");
                Log.e(TAG, e.toString());
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
            Log.d(TAG, "onServiceDisconnected");
        }
    };

    /**
     *  sends this instance to accelerationService, so that it can receive movement messages
     */
    private void registerSelf() {
        if (!serviceBound)
            return;
        accelerationService.registerClient(this);
    }


    /**
     * Parses Actions from notification key presses
     * @param intent intent with action
     * @param flags don't know
     * @param startId don't care
     * @return Always returns START_STICKY so that OS doesn't mess with this service
     */

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        String action = intent.getAction();
        Log.d(TAG, action);

        if (action == null) {
            return START_STICKY;
        }

        if (action.equals(ACTION_STOP)) {
            Log.d(TAG, "received stop command");
            Stop();
        }

        if (action.equals(ACTION_PLAY_PAUSE)) {
            Log.d(TAG, "received play/pause command");
            if (isPaused)
                Play();
            else
                Pause();
        }

        if (action.equals(ACTION_EXIT)) {
            Exit();
        }

        return START_STICKY;
    }

    public void gesturesOn() {
        if (serviceBound)
            return;
        if (startAndBindAccelerationService())
            Toast.makeText(this, R.string.gestures_on_toast, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, R.string.gestures_on_error_toast, Toast.LENGTH_SHORT).show();
    }

    public void gesturesOff() {
        if (!serviceBound)
            return;
        if(stopAndUnbindAccelerationService())
            Toast.makeText(this, R.string.gestures_off_toast, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, R.string.gestures_off_error_toast, Toast.LENGTH_SHORT).show();
    }

    /**
     * Starts AccelerationService and binds it to the Binder
     * @return returns true if the service is successfully started and bound
     */
    private boolean startAndBindAccelerationService() {
        if (serviceBound)
            return true;
        try {
            Log.d(TAG, "starting and binding accelerationService");
            Intent accelerationServiceIntent = new Intent(this, AccelerationService.class);
            startService(accelerationServiceIntent);
            bindService(accelerationServiceIntent, accelerationServiceConnection, 0);
            serviceBound = true;
            return true;
        } catch (Exception e ) {
            Log.e(TAG,"starting and binding service failed");
            Log.e(TAG, e.toString());
            return false;
        }
    }

    /**
     * stops and unbinds the AccelerationService
     * @return returns true service is successfully stopped and unbound
     */
    private boolean stopAndUnbindAccelerationService() {
        if (!serviceBound)
            return true;
        try {
            stopService(new Intent(this, AccelerationService.class));
            unbindService(accelerationServiceConnection);
            serviceBound = false;
            return true;
        } catch (Exception e ) {
            Log.e(TAG, "stopping and unbinding acceleration service failed");
            Log.e(TAG, e.toString());
            return false;
        }
    }

    /**
     * Stores the instance of MainActivity so that it can update the UI and stop it on exit
     * @param activity MainActivity instance that starts the service
     */
    public void registerClient(MainActivity activity) {
        try{
            Log.d(TAG, "registerClient");
            mainActivity = activity;
        } catch (Exception e) {
            Log.e(TAG, "Error getting MainActivity");
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        songs = getSongsList();

        // This service needs songs to operate. If there are none, it stops itself
        if (songs.length == 0) {
            Toast.makeText(this, R.string.no_songs_toast, Toast.LENGTH_SHORT).show();
            Log.d(TAG, "no songs were found in the assets folder");
            stopSelf();
        }

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //notificationBuilder = new NotificationCompat.Builder(this, channelID);
        createNotificationChannel();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        if (audioPlayer != null) {
            audioPlayer.release();
        }

        if (serviceBound)
            stopAndUnbindAccelerationService();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "returning binder");
        return serviceBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "Getting unbound");
        return super.onUnbind(intent);
    }

    /**
     * Binder for MainActivity to use to attach this service to itself
     */
    class MediaPlayerBinder extends Binder {
        MediaPlayerService getService() {
            return MediaPlayerService.this;
        }
    }

    /**
     * Starts/continues playing the song
     */
   public void Play()  {
       // if song is already playing just quit
       if (audioPlayer != null && audioPlayer.isPlaying()) {
           Log.d(TAG, "a song is already playing");
           return;
       }

       // if song is paused continue playing
       if (audioPlayer != null && isPaused) {
           audioPlayer.start();
           isPaused = false;
           songUIHandler.sendEmptyMessage(MSG_UPDATE_TIME);

           // set notification with PAUSE button instead of PLAY
           notificationManager.notify(NOTIFICATION_ID, createNotification());
           return;
       }

        // if song is not playing nor paused play new song
        Log.d(TAG, "startPlaying");
        currentSong = getRandomSong();
        AssetFileDescriptor afd;
        try {
            afd = getAssets().openFd(currentSong);
            audioPlayer = new MediaPlayer();
            audioPlayer.setOnCompletionListener(mp -> Stop());
            audioPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            audioPlayer.prepare();
            audioPlayer.start();
            isPaused = false;
        } catch (IOException e) {
            Log.e(TAG,"Error getting song info and playing song");
            Log.e(TAG,e.toString());
        }

        songUIHandler.sendEmptyMessage(MSG_UPDATE_TIME);

        // set notification
        notificationManager.notify(NOTIFICATION_ID, createNotification());
   }

   public int getSongDuration() {
        try {
            return audioPlayer.getDuration();
        } catch (Exception e ) {
           return -1;
        }
   }

   public int getSongPosition() {
        try {
            return audioPlayer.getCurrentPosition();
        } catch (Exception e ) {
            return -1;
        }
   }

   public String getCurrentSong() {
        return currentSong == null ? getString(R.string.song_name_placeholder) : currentSong;
   }

    /**
     * Pauses playback
     */
   public void Pause() {
       Log.d(TAG, "pausePlaying");
       if (audioPlayer == null || isPaused)
           return;
       isPaused = true;
       audioPlayer.pause();

       // stop using resources to change UI when music is paused and there is no change
       songUIHandler.removeMessages(MSG_UPDATE_TIME);

       // make a new notification, which will have a PLAY button instead of the PAUSE one
       notificationManager.notify(NOTIFICATION_ID, createNotification());
   }

    /**
     * Stops playback
     */
   public void Stop() {
       Log.d(TAG, "stopPlaying");
       if (audioPlayer == null)
           return;
       audioPlayer.stop();
       isPaused = true;
       currentSong = null;
       audioPlayer.release();
       songUIHandler.removeMessages(MSG_UPDATE_TIME);

       // Set new notification with PLAY button instead of the PAUSE button
       notificationManager.notify(NOTIFICATION_ID, createNotification());

       // this prevents IllegalStateError when checking if audioPlayer is playing
       audioPlayer = null;

       // displays UI and notification texts back to original placeholders
       updateSongInfo();
       updateSongDuration();
   }

    /**
     * Stops all running services, Activity, Message Handlers and removes notification. Stops music playback
     */
   public void Exit() {
       Stop();
       notificationManager.cancel(NOTIFICATION_ID);
       stopAndUnbindAccelerationService();
       mainActivity.finish();
   }


    /**
     * Gets names of all mp3 files in the assets directory
     * @return list of song names
     */
   private String[] getSongsList() {
       String[] files;
       try {
           Pattern mp3Regex = Pattern.compile(".*.mp3$");
           files = Arrays.stream(getAssets().list(""))
                   .filter(file -> mp3Regex.matcher(file).matches())
                   .toArray(String[]::new);
       } catch (IOException e) {
           files = null;
           Log.e(TAG, "error reading files");
           Log.e(TAG, e.toString());
       }

       return files;
   }

    /**
     * Randomly choose next song
     * @return name of chosen song
     */
   private String getRandomSong() {
        Random rand = new Random();
        return songs[rand.nextInt(songs.length)];
   }

    /**
     * Parse duration in milliseconds to String of minutes and seconds
     * @param millis duration in milliseconds
     * @return duration in format mm:ss
     */
   @NonNull
    private String parseMilliSeconds(int millis) {
        int minutes = (millis / 1000) / 60;
        int seconds = (millis / 1000) % 60;

        //return minutes + ":" + seconds;
        return String.format("%d:%02d", minutes, seconds);
    }

    /**
     * Updates song name in UI and notification
     */
    private void updateSongInfo() {
        String songName = getCurrentSong();
        if (songName == null)
            songName = getString(R.string.song_name_placeholder);

        // update UI
        try {
            mainActivity.songInfo.setText(songName);
        } catch (Exception e) {
            Log.e(TAG, "Error changing song name in UI");
            Log.e(TAG, e.toString());
        }

        //update notification
       if (notificationBuilder != null)  {
           notificationBuilder.setContentTitle(getCurrentSong());
           notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
       }
    }


    /**
     * Updates duration ond position information in UI and notification
     */
    private void updateSongDuration() {
        if (mainActivity == null)
            return;
        int totalDuration = getSongDuration();
        int currentProgress = getSongPosition();

        songDuration = totalDuration == -1 || currentProgress == -1 ? getString(R.string.song_duration_placeholder) : parseMilliSeconds(currentProgress) + "/" + parseMilliSeconds(totalDuration);

        //update UI
        if (mainActivity == null)
            return;
        try {
            mainActivity.songDuration.setText(songDuration);
        } catch (Exception e) {
            Log.e(TAG, "Error changing song duration");
            Log.e(TAG, e.toString());
        }

        // update notification
        if (notificationBuilder != null) {
            notificationBuilder.setContentText(songDuration);
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
        }
    }

    /**
     * Handler for updating song information every second
     */
    static class SongUIHandler extends Handler {
        private final WeakReference<MediaPlayerService> service;
        private final String TAG = SongUIHandler.class.getSimpleName();

        SongUIHandler(MediaPlayerService service) {
            this.service = new WeakReference<MediaPlayerService>(service);
        }

        @Override
        public void handleMessage(Message message) {
            if (message.what == MSG_UPDATE_TIME) {
                Log.d(TAG, "updating time");
                service.get().updateSongInfo();
                service.get().updateSongDuration();
                int UPDATE_RATE_MS = 1000;
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS);
            }
        }
    };

    /**
     * Creates notification which displays song information and offers clicking things
     * @return are you even reading this?
     */
    private Notification createNotification() {
        Intent stopIntent = new Intent(this, MediaPlayerService.class);
        stopIntent.setAction(ACTION_STOP);
        Intent exitIntent = new Intent(this, MediaPlayerService.class);
        exitIntent.setAction(ACTION_EXIT);

        Intent playPauseIntent = new Intent(this, MediaPlayerService.class);
        playPauseIntent.setAction(ACTION_PLAY_PAUSE);


        PendingIntent stopPendingIntent = PendingIntent.getService(this, 0, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent exitPendingIntent = PendingIntent.getService(this, 0, exitIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent playPausePendingIntent = PendingIntent.getService(this, 0, playPauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Log.d(TAG, "Is Paused: " + isPaused);
        //notificationBuilder = new NotificationCompat.Builder(this, channelID)

        notificationBuilder = new NotificationCompat.Builder(this, channelID)
                .setContentTitle(getCurrentSong())
                .setContentText(songDuration)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setChannelId(channelID)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .addAction(android.R.drawable.ic_media_pause, isPaused ? getString(R.string.play_button_text) : getString(R.string.pause_button_text), playPausePendingIntent)
                .addAction(android.R.drawable.ic_media_pause, getString(R.string.stop_button_text), stopPendingIntent)
                .addAction(android.R.drawable.ic_media_play, getString(R.string.exit_button_text), exitPendingIntent);


        // return to MainActivity when clicking on notification
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(mainActivity, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(resultPendingIntent);

        // return built notification
        return notificationBuilder.build();
    }

    /**
     * Creates the notification channel where the notifications will be posted
     * I'm actually doing this just so that I don't lose the points.
     * I like your smile :)
     */
    private void createNotificationChannel() {
        NotificationChannel channel = new NotificationChannel(channelID, "name", NotificationManager.IMPORTANCE_LOW);
        // I have no idea what half of these things even do
        channel.setDescription("description");
        channel.enableLights(true);
        channel.setLightColor(Color.RED);
        channel.enableVibration(true);

        notificationManager.createNotificationChannel(channel);
    }
}
